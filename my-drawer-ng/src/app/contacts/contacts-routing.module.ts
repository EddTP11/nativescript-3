import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { ContactsComponent } from "./contacts.component";
import { FavoritesComponent } from "./favorites.component";

const routes: Routes = [
    { path: "", component: ContactsComponent },
    { path: "favorites", component: FavoritesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ContactsRoutingModule { }
